========================
README: PETSc for Python
========================

:Author:       Lisandro Dalcin
:Contact:      dalcinl@gmail.com
:Organization: CIMEC <http://www.cimec.org.ar/>
:Address:      CCT CONICET, 3000 Santa Fe, Argentina

Thank you for downloading the *PETSc for Python* project archive. As
this is a work in progress, please check the `project website`_ for
updates.

.. _CIMEC:            http://www.cimec.org.ar/
.. _project website:  https://bitbucket.org/petsc/petsc4py/


- To build and install this package, you must meet the following
  requirements.

  + PETSc_ 3.4 or 3.3, built with *shared libraries* (i.e., by
    passing ``--with-shared-libraries`` option to PETSc ``configure``
    script; this is not strictly required, but **highly** recommended).

  + Python_ 2.4 to 2.7 and 3.1 to 3.3.

  + NumPy_ 1.0.1 and above.

.. _PETSc:  http://www.mcs.anl.gov/petsc/
.. _Python: http://www.python.org
.. _NumPy:  http://www.numpy.org


- This package uses standard distutils_. For detailed instructions
  about requirements and the building/install process, read the file
  ``docs/source/install.rst``.

.. _distutils: http://docs.python.org/distutils/
